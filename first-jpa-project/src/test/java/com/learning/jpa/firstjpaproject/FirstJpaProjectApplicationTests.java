package com.learning.jpa.firstjpaproject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FirstJpaProjectApplicationTests {

	@Test
	void contextLoads() {
	}

	private CustomerMapper mapper = Mappers.getMapper(CustomerMapper.class);

	@Test
	public void givenSourceToDestination_whenMaps_thenCorrect() {
		CustomerDto dto = new CustomerDto();
		dto.setFirstName("test");
		List<AccountDto> accountList = new ArrayList<AccountDto>();
		AccountDto accountDto = new AccountDto("testname");
		accountList.add(accountDto);
		dto.setAccounts(accountList);
		Customer entity = mapper.customerDtoToCustomer(dto);
		assertEquals(dto.getAccounts().get(0).getName(), entity.getAccounts().get(0).getName());
		assertEquals(dto.getFirstName(), entity.getFirstName());
	}

}
