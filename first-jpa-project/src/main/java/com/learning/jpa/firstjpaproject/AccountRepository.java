package com.learning.jpa.firstjpaproject;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

	List<Account> findByName(String lastName);

	Account findById(long id);
}