package com.learning.jpa.firstjpaproject;

import org.mapstruct.Mapper;

@Mapper
public interface CustomerMapper {
	CustomerDto customerToCustomerDto(Customer source);

	Customer customerDtoToCustomer(CustomerDto destination);
}
