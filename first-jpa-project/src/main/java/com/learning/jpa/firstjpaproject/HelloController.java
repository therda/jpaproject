package com.learning.jpa.firstjpaproject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/test")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}