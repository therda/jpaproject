package com.learning.jpa.firstjpaproject;

import org.mapstruct.Mapper;

@Mapper
public interface AccountMapper {
	AccountDto accountToAccountDto(Account source);

	Account accountDtoToAccount(AccountDto destination);
}
