package com.learning.jpa.firstjpaproject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FirstJpaProjectApplication {

	private static final Logger log = LoggerFactory.getLogger(FirstJpaProjectApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(FirstJpaProjectApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository, AccountRepository accountReposoitory) {
		return (args) -> {
			
			accountReposoitory.save(new Account("test1"));
			accountReposoitory.save(new Account("test2"));
			
			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Account account : accountReposoitory.findAll()) {
				log.info(account.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Account account = accountReposoitory.findById(1L);
			log.info("Customer found with findById(1L):");
			log.info("--------------------------------");
			log.info(account.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			accountReposoitory.findByName("test1").forEach(accountInLoop -> {
				log.info(accountInLoop.toString());
			});
			log.info("");
			
			// save a few customers
			customerRepository.save(new Customer("Jack", "Bauer"));
			customerRepository.save(new Customer("Chloe", "O'Brian"));
			customerRepository.save(new Customer("Kim", "Bauer"));
			customerRepository.save(new Customer("David", "Palmer"));
			Customer customerTest = customerRepository.save(new Customer("Michelle", "Dessler"));
			customerTest.getAccounts().add(account);
			customerRepository.save(customerTest);
			
			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : customerRepository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
//			Customer customer = customerRepository.findById(1L);
//			log.info("Customer found with findById(1L):");
//			log.info("--------------------------------");
//			log.info(customer.toString());
//			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			customerRepository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});
			log.info("");
		};
	}
}
